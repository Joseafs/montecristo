<div class="container">
    <div id="empreendimento" class="content pdg20 tCenter">
        <div class="floatL w30 md-w100 default pdg20 pdg60T sm-pdg5 sm-mgn0 mgn50T fSize16 cSecond md-pdg0 md-mgn0">
            A �nica �rea que eu acho, que vai exigir muita aten��o nossa, e a� eu j� aventei a hip�tese de at� criar um minist�rio. � na �rea de... Na �rea... Eu diria assim, como uma esp�cie de analogia com o que acontece na �rea agr�cola.
            Eu dou dinheiro pra minha filha. Eu dou dinheiro pra ela viajar, ent�o �... �... J� vivi muito sem dinheiro, j� vivi muito com dinheiro. -Jornalista: Coloca esse dinheiro na poupan�a que a senhora ganha R$10 mil por m�s. -Dilma: O que que � R$10 mil?
        </div>
        <div class="contIcon floatR w60 md-w100 pdg20T">
            <div class="box6 pdg3 md-w25 sm-w50 pdg3 md-w25">
                <a class="mcristo-icon-exit" alt='' title='' href='#'  ></a>
                <h3 class="title fNormal cSecond">Portaria</h3>
            </div>
            <div class="box6 pdg3 md-w25 sm-w50">
                <a class="mcristo-icon-food-1" alt='' title='' href='#'  ></a>
                <h3 class="title fNormal cSecond">Sal�o de Festas</h3>
            </div>
            <div class="box6 pdg3 md-w25 sm-w50">
                <a class="mcristo-icon-food" alt='' title='' href='#'  ></a>
                <h3 class="title fNormal cSecond">Espa�o Gourmet</h3>
            </div>
            <div class="box6 pdg3 md-w25 sm-w50">
                <a class="mcristo-icon-fun" alt='' title='' href='#'  ></a>
                <h3 class="title fNormal cSecond">Play Ground</h3>
            </div>
            <div class="box6 pdg3 md-w25 sm-w50">
                <a class="mcristo-icon-tool" alt='' title='' href='#'  ></a>
                <h3 class="title fNormal cSecond">Aquecedor Solar</h3>
            </div>
            <div class="box6 pdg3 md-w25 sm-w50">
                <a class="mcristo-icon-farm" alt='' title='' href='#'  ></a>
                <h3 class="title fNormal cSecond">Po�o Artesiano</h3>
            </div>
            <div class="box6 pdg3 md-w25 sm-w50">
                <a class="mcristo-icon-sofa" alt='' title='' href='#'  ></a>
                <h3 class="title fNormal cSecond">Sala de Estar</h3>
            </div>
            <div class="box6 pdg3 md-w25 sm-w50">
                <a class="mcristo-icon-shapes" alt='' title='' href='#'  ></a>
                <h3 class="title fNormal cSecond">Sala de Jantar</h3>
            </div>
            <div class="box6 pdg3 md-w25 sm-w50">
                <a class="mcristo-icon-kitchen" alt='' title='' href='#'  ></a>
                <h3 class="title fNormal cSecond">Lavabo</h3>
            </div>
            <div class="box6 pdg3 md-w25 sm-w50">
                <a class="mcristo-icon-kitchen-1" alt='' title='' href='#'  ></a>
                <h3 class="title fNormal cSecond">Cozinha</h3>
            </div>
            <div class="box6 pdg3 md-w25 sm-w50">
                <a class="mcristo-icon-sleeping" alt='' title='' href='#'  ></a>
                <h3 class="title fNormal cSecond">2 Quartos</h3>
            </div>
            <div class="box6 pdg3 md-w25 sm-w50">
                <a class="mcristo-icon-rest" alt='' title='' href='#'  ></a>
                <h3 class="title fNormal cSecond">1 Suite</h3>
            </div>
            <div class="box6 pdg3 md-w25 sm-w50 floatOff dInlineB">
                <a class="mcristo-icon-transport" alt='' title='' href='#'  ></a>
                <h3 class="title fNormal cSecond">Garagem</h3>
            </div>
        </div>
    </div>
</div>
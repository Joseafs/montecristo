
<div class="floatL w100 zInd2 pRelative" style="margin-bottom: 250px;">
    <div class="container">
        <div class="content pdg30B">
            <h1 class="title cSecond sm-tCenter">Contato</h1>
            <form class="formInfo floatL w50 sm-w100 pdg20B" method="POST" action="" >
                <div class="inputField w100 pdg3">
                    <input id="iptNome" type="text" placeholder="Nome" name="nome" class="selectField bgOpac-dark1" required>
                </div>
                <div class="inputField w60 pdg3">
                    <input id="iptEmail" type="email" placeholder="E-mail" name="email" class="selectField bgOpac-dark1" required>
                </div>
                <div class="inputField w40 pdg3">
                    <input id="iptFone" type="tel" placeholder="Telefone" name="telefone" class="selectField bgOpac-dark1 fone" required>
                </div>
                <div class="inputField w60 pdg3">
                    <input id="iptCidade" type="text" placeholder="Cidade" name="cidade" class="selectField bgOpac-dark1" required>
                </div>
                <div class="inputField w40 pdg3">
                    <select id="sltEstado" name="estado" class="selectField bgOpac-dark1 cGray3" required>
                        <option value="" class="displayOff">Estado</option>
                        <?php
                        ?>
                    </select>
                </div>
                <div class="inputField w100 pdg3">
                    <input id="iptAssunto" type="text" placeholder="Assunto" name="assunto" class="selectField bgOpac-dark1" required>
                </div>
                <div class="inputField w100 pdg3 pRelative pdg60R" >
                    <textarea id="txtMessage" placeholder="Mensagem" name="mensagem" class="selectField bRad3L bgOpac-dark1"></textarea>
                    <button name="btnEnviar" value="Enviar" class="bgPrimary bRad3R cWhite effRipple floatR effShadow pntPointer pRelative" style="width: 57px;" type="submit" >
                        <span class="pdg5 w100 floatL clearB txt14">Enviar</span>
                    </button>
                </div>
            </form>
            <div class="floatR w50 pdg20L tCenter sm-w100 mgn20B sm-pdg0">
                <h3 class="title cPrimary">Utilize o formulário, ou se preferir:</h3>
                <div class="default floatL fSize16 w100 pdg8 tLeft">
                    <?php
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
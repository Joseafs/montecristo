<div class='js-flickity floatL w100'  data-flickity-options='{"freeScroll": true, "cellAlign": "left", "wrapAround": true, "pageDots": false, "prevNextButtons": false}'>
    <!--; Leva um container/content 'efeito slide' -->   
    <div class='pdg3 gallery-cell'>
        <div class="portaria content pRelative">
            <div class='gumPos-txt default fSize14 cSecond tLeft'>
                &bull; Entrada e sa�da excluisiva para moradores <br />
                &bull; "N�o, est� em andamento!" <br /> 
                &bull; Agora, n�o come�amos a fazer. <br />
            </div>
            <div class="galeria" >
                <figure class="gumPos-img1">
                    <a href="img/foto01.png" itemprop="contentUrl" data-size="768x432"  itemprop="associatedMedia" itemscope >
                        <img class='max-w100' src="img/folha01.png" itemprop="thumbnail" alt="Image description" />
                    </a>
                </figure>
                <figure class="gumPos-img2">
                    <a href="img/foto02.png" itemprop="contentUrl" data-size="768x432"  itemprop="associatedMedia" itemscope >
                        <img class='max-w100' src="img/folha02.png" itemprop="thumbnail" alt="Image description" />
                    </a>
                </figure>
            </div>
            <div class="gumPos-mcristo-portaria"></div>
            <div class="gumPos-mcristo-icon-shapes-1"></div>
        </div>
    </div>
    <div class='pdg3 gallery-cell'>
        <div class="salao-festas content pRelative" >
            <div class='gumPos-txt default fSize14 cSecond tLeft'>
                &bull; Estrutura completa para os eventos <br />
                &bull; Controle de calend�rio <br /> 
                &bull; Insfraestrutura para buffet <br />
            </div>
            <div class="galeria" >
                <figure class="gumPos-img1">
                    <a href="img/foto03.png" itemprop="contentUrl" data-size="991x557"  itemprop="associatedMedia" itemscope >
                        <img class='max-w100' width="615" src="img/folhas03.png" itemprop="thumbnail" alt="" />
                    </a>
                </figure>
            </div>

            <div class="gumPos-mcristo-festas"></div>
            <div class="gumPos-mcristo-icon-food-1"></div>
        </div>
    </div>
    <div class='pdg3 gallery-cell'>
        <div class="playground content pRelative">
            <div class='gumPos-txt default fSize14 cSecond tLeft'>
                &bull; Gangorra <br />
                &bull; Escorregador <br /> 
                &bull; Gira-Gira <br />
                &bull; Balan�o <br />
            </div>
            <div class='gumPos-txt2 default fSize14 cSecond tLeft'>
                O <strong>RESIDENCIAL MONTE CRISTO </strong> � um dos poucos condom�nios fechados em Paranava� com toda a documenta��o aprovada antes do lan�amento.
            </div>
            <div class="galeria" >
                <figure class="gumPos-img1">
                    <a href="img/foto05.png" itemprop="contentUrl" data-size="739x416"  itemprop="associatedMedia" itemscope >
                        <img class='max-w100' src="img/folhas08.png" itemprop="thumbnail" alt="Image description" />
                    </a>
                </figure>
                <figure class="gumPos-img2 ">
                    <a href="img/foto06.png" itemprop="contentUrl" data-size="779x439"  itemprop="associatedMedia" itemscope >
                        <img class='max-w100' src="img/folhas09.png" itemprop="thumbnail" alt="Image description" />
                    </a>
                </figure>
            </div>
            <div class="gumPos-mcristo-playground"></div>
            <div class="gumPos-mcristo-icon-fun"></div>
        </div>
    </div>
    <div class='pdg3 gallery-cell'>
        <div class="fachada content pRelative">
            <div class='gumPos-txt default fSize14 cSecond tLeft'>
                &bull; Casas Planejadas <br />
                &bull; Garagem Coberta <br /> 
                &bull; Sacada <br />
            </div>
            <div class="galeria" >
                <figure class="gumPos-img1">
                    <a href="img/foto06.png" itemprop="contentUrl" data-size="739x416"  itemprop="associatedMedia" itemscope >
                        <img class='max-w100' src="img/folhas10.png" itemprop="thumbnail" alt="Image description" />
                    </a>
                </figure>
                <figure class="gumPos-img2 ">
                    <a href="img/foto07.png" itemprop="contentUrl" data-size="779x439"  itemprop="associatedMedia" itemscope >
                        <img class='max-w100' src="img/folhas11.png" itemprop="thumbnail" alt="Image description" />
                    </a>
                </figure>
            </div>
            <div class="gumPos-mcristo-fachada"></div>
            <div class="gumPos-mcristo-icon-rest"></div>
        </div>
    </div>
</div>

<div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="pswp__bg"></div>
    <div class="pswp__scroll-wrap">
        <div class="pswp__container">
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
        </div>
        <div class="pswp__ui pswp__ui--hidden">
            <div class="pswp__top-bar">
                <div class="pswp__counter"></div>
                    <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>
                    <button class="pswp__button pswp__button--share" title="Share"></button>
                    <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>
                    <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>
                <div class="pswp__preloader">
                    <div class="pswp__preloader__icn">
                        <div class="pswp__preloader__cut">
                            <div class="pswp__preloader__donut"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                <div class="pswp__share-tooltip"></div>
            </div>
            <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)"></button>
            <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)"></button>
            <div class="pswp__caption">
                <div class="pswp__caption__center"></div>
            </div>
        </div>
    </div>
</div>
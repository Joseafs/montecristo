<header id="header-content" class='floatL w100 zInd99 live-1s'>
    <div id="header-toggle" class="effRippleDark ovflwH effShadow floatR bgFirst pdg8 pdg20T tCenter pntPointer gt-displayOff">
        <div class="effToggle">
            <div class="has-toggle"></div>
            <div class="has-toggle"></div>
            <div class="has-toggle"></div>
        </div>
    </div>
    <div class='container'>
        <div class='content'>
            <div id="header-logo" class="floatL md-w100 tCenter">
                <a class='dInlineB bRad3 ovflwH effRipple' href='?cod=home' title='' ><img class='floatL' width="200" src='img/logo_mcristo.svg' alt='' /></a>
            </div>
            
                <script>
                    
                    $(function() {

                    });
                </script>
            
            <div id="header-menu" class="tCenter pdg8 floatR fSize20 pdg20T md-w100" >
                <ul class='contMenu floatL w100'>
                    <li class="w20 md-w100" >
                        <a class='linkMenu effRipple linkDelay cFirst' data-nav='empreendimento' href='?cod=empreendimento' alt='Empreendimento' title='Empreendimento' >O empreendimento</a>
                    </li>
                    <li class="w20 md-w100" >
                        <a class='linkMenu effRipple linkDelay cFirst' data-nav='infraestrutura' href='?cod=infraestrutura' alt='Infraestrutura' title='Infraestrutura' >Infraestrutura</a>
                    </li>
                    <li class="w20 md-w100" >
                        <a class='linkMenu effRipple linkDelay cFirst' data-nav='video' href='?cod=video' alt='V�deo' title='V�deo' >V�deo</a>
                    </li>
                    <li class="w20 md-w100" >
                        <a class='linkMenu effRipple linkDelay cFirst' data-nav='contato' href='?cod=contato' alt='Contato' title='Contato' >Contato</a>
                    </li>
                    <li class="w20 md-w100">
                        <a class="linkMenu effRipple fBold cFirst" alt="" title='Discar <?php //=$config->getConfig(6)?>'  href='tel:<?php //=$config->getConfig(6)?>' target="_top">(44) - 9999.6666</a>
                    </li>
                </ul>
                <div class='navLine bgFirst md-displayOff'>
                    <a class='navDot linkDelay' data-nav='empreendimento' href="?cod=empreendimento"></a>
                    <a class='navDot linkDelay' data-nav='infraestrutura' href="?cod=infraestrutura"></a>
                    <a class='navDot linkDelay' data-nav='video' href="?cod=video"></a>
                    <a class='navDot linkDelay' data-nav='contato' href="?cod=contato"></a>

                    <div class="navDot navMarker">
                        <svg class='w100 floatL' version="1.1"  xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 84 85" enable-background="new 0 0 84 85" xml:space="preserve">
                            <path  class='fillSecond' d="M41.869,0c0,0-44.874,31.546-41.709,57.644c4.473,33.818,76.902,39.037,83.233,0 C86.371,29.957,41.869,0,41.869,0z"/>
                        </svg>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>

<?php
    //include_once("_cabecalho.php");
?>
<!DOCTYPE html>
<html>
     <head>
        <!-- 21/03/2016 -->
        <title> MONTE CRISTO - RESIDENCIAL </title>
        <meta http-equiv="content-type" content="text/html; charset=windows-1252" />
        <meta name="keywords" content=" " />
        <meta name="Description" content="MONTE CRISTO - RESIDENCIAL" />
        <meta name="author" content="Uses - Design & Software - www.uses.com.br" />
        <meta name="google-site-verification" content="F-S2FOnqEpdHKfDUN7YiT4Q-mUYiHNll3uL4TEPvXAA" />
        <meta name="robots" content="noindex, nofollow" />
        
        <meta property="og:title" content="MONTE CRISTO - RESIDENCIAL" />
        <meta property="og:url" content="http://<?=$_SERVER['HTTP_HOST']; ?>" />
        <meta property="og:site_name" content="MONTE CRISTO - RESIDENCIAL" />
        <meta property="og:image" content="http://<?=$_SERVER['HTTP_HOST']; ?>/img/topoLogo.png" />
        <meta property="og:description" content="MONTE CRISTO - RESIDENCIAL" />
        <meta name="viewport" content="width=device-width, initial-scale=1 , minimum-scale=0.5 ,maximum-scale=2"> 
        
        <!-- Chrome, Firefox OS and Opera -->
        <meta name="theme-color" content="#E3C692">
        <!-- Windows Phone -->
        <meta name="msapplication-navbutton-color" content="#E3C692">
        <!-- iOS Safari -->
        <meta name="apple-mobile-web-app-status-bar-style" content="#E3C692">
        
        <link rel="apple-touch-icon" sizes="57x57" href="img/favicon/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="img/favicon/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="img/favicon/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="img/favicon/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="img/favicon/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="img/favicon/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="img/favicon/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="img/favicon/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="img/favicon/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192"  href="img/favicon/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="img/favicon/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="img/favicon/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="img/favicon/favicon-16x16.png">
        <link rel="manifest" href="img/favicon/manifest.json">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="img/favicon/ms-icon-144x144.png">
        <meta name="theme-color" content="#ffffff">

        <link type="text/css" rel="stylesheet" href="css/aparencia.css?<?=time();?>" >
        <link type="text/css" rel="stylesheet" href="css/jquery.materialripple.css" >
        <link type="text/css" rel="stylesheet" href="css/efeitos.css?<?=time();?>">
        
        <link rel="stylesheet" href="css/flickity.css"> 
        <link rel="stylesheet" href="css/photoswipe.css"> 
        <link rel="stylesheet" href="css/photoswipe-default.css"> 

        <link type="text/css" rel="stylesheet" href="css/responsivo.css?<?=time();?>" > 
        <link type="text/css" rel="stylesheet" href="css/mobile.css?<?=time();?>" media="all and (max-width: 768px)" title="Nexus Low">
        
        <script type="text/javascript" src="js/jquery-1.8.3.min.js"></script>
        <!--
            <script>
                (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
                ga('create', 'UA-7493429-82', 'auto');
                ga('send', 'pageview');
            </script>
        -->
    </head>
    
    <body >
        <div id="fb-root"></div>
        <?php include_once 'topo.php'; ?>
        <section class="floatL w100">
            <?php if(!file_exists($_GET["cod"].".php") || $_GET["cod"]=="" || is_numeric($_GET["cod"]) || $_GET["cod"]=="home"){
                    include_once ("home.php");
                    $_GET["cod"] = 'home';

                } else {
                    include_once ($_GET["cod"].".php");
                }
            ?>
        </section>

        <?php // include_once 'rodape.php'; ?>
        
        <script src="https://apis.google.com/js/platform.js" async defer>
            {lang: 'pt-BR'}
        </script>

        <script type="text/javascript" src="js/jquery.materialripple.js"></script>
        <script type="text/javascript" src="js/swipe.js"></script>
        <script type="text/javascript" src="js/jquery.maskedinput-1.3.1.min.js"></script>
        
        <script src="js/flickity.js"></script> 
        
        <script src="js/photoswipe.min.js"></script> 
        <script src="js/photoswipe-ui-default.js"></script> 
        
        <script type="text/javascript" src="js/script.js"></script>
        
        <script>
            kibinLoader('<?=$_GET['cod']?>');
        </script>
    </body>
</html>
<?php
    //$conn->desconectar();
?>

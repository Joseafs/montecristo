var player;

function onYouTubePlayerAPIReady() {
    player = new YT.Player('player', {
        playerVars: {
            'autoplay': 1,
            'controls': 0,
            //'autohide': 1,
            //'wmode': 'opaque',
            showinfo:0,
            'loop': 1,
            //'start': 15,
            //'end': 110,
        },
        videoId: 'pg7ofsVOGsw',
        events: {
            'onReady': onPlayerReady
        }
    });

}

function onPlayerReady(event) {
    event.target.mute();
    //why this? Well, if you want to overlay text on top of your video, you
    //will have to fade it in once your video has loaded in order for this
    //to work in Safari, or your will get an origin error.
}


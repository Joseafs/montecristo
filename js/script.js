/* -------------------------------------------------------------------------- */
$("#btnScrollTop").click(function() {
    $('html, body').animate({scrollTop: 0}, 1500);
});

$('.effRipple, .effRippleDark, .contMenu a.effActv, .subMenu a').materialripple();

$('.btnActv').click(function(e) {
    e.preventDefault();
    $(this).addClass('active');
});
/* -------------------------------------------------------------------------- */
$("#header-toggle").click(function(){
    $("#header-toggle, #header-toggle .effToggle").toggleClass('active');
    $("#header-content").toggleClass('active');
    $("body").toggleClass('ovflwH');
});
/* -------------------------------------------------------------------------- */
$(".btnModal, .wrapShadow, .closeModal").click(function(){
    
    var modal = $(this).data('target');
    if($(modal).hasClass('active')) {
        
        $(modal).fadeOut().removeClass('active');
        $("body").removeClass('modal-open');
    }else {
        $('.modal').fadeOut().removeClass('active');
        
        // Add active class to sct title
        $(modal).addClass('active').fadeIn();
        $("body").addClass('modal-open');
    }
});
/* -------------------------------------------------------------------------- */
function kibinLoader(codSessao){

    if(codSessao  == 'empreendimento'){
        $(".navMarker").css('left', '10%').addClass('active');
    } else if(codSessao  == 'infraestrutura'){ 
        $(".navMarker").css('left', '30%').addClass('active');
    } else if(codSessao  == 'video'){ 
        $(".navMarker").css('left', '50%').addClass('active');
    } else if(codSessao  == 'contato'){ 
        $(".navMarker").css('left', '70%').addClass('active');
    } else{ 
        $(".navMarker").addClass('home');
    }
}

$("[data-nav]").click(function(){
    
    var clickSessao = $(this).data('nav');

    kibinLoader(clickSessao);
});
/*
    $("[data-nav]").mouseover(function(){
        $(".navMarker").addClass('inHover');
    });
    $("[data-nav]").mouseleave(function(){
        $(".navMarker").removeClass('inHover');
    });
*/
function close_modal() {
    $('a.acd-sct-title, a.acd-title-icon').removeClass('active');
    $('.acd-sct-content').slideUp(300).removeClass('open');
}
$('a.acd-sct-title, a.acd-title-icon').click(function(e) {
    // Grab current anchor value
    var currentAttrValue = $(this).attr('href');
    if($(e.target).is('.active')) {
        close_modal();
    }else {
        close_modal();
        // Add active class to sct title
        $(this).addClass('active');
        // Open up the hidden content panel
        $(".acd " + currentAttrValue).slideDown(300).addClass('open');
    }
    e.preventDefault();
});

/* -------------------------------------------------------------------------- */
/* Form */  
$("input.data").mask("99/99/9999");
$("input.cep").mask("99999-999");
$("input.n_casa").mask("9999999");
$("input.cpf").mask("999.999.999.99");
$("input.rg").mask("99.999.999.9");
$("input.fone").focusout(function(){
    var phone, element;
    element = $(this);
    element.unmask();
    phone = element.val().replace(/\D/g, '');
    if(phone.length > 10) {
        element.mask("(99) 99999-999?9");
    } else {
        element.mask("(99) 9999-9999?9");
    }
}).trigger('focusout');
   
/* -------------------------------------------------------------------------- */
/* Rolagem */
$('a.linkMove').on('click', function(event) {
    var target = $( $(this).attr('href') );
    if( target.length ) {
        event.preventDefault();
            var y = $(window).width();{
                if ( y <= (1024)){
                    $("#menu1").slideUp(500);
                }
            };
        $('html, body').animate({
            scrollTop: target.offset().top -40
        }, 1000);
    }
});
    
/* -------------------------------------------------------------------------- */
/* Blinds */
$(".clickOff").bind("contextmenu", function(e) {
    e.preventDefault();
});

$("a.linkNull").click(function() {
   
    return false;
});

$('a.linkDelay').click(function (e) {
    
    e.preventDefault();                   // prevent default anchor behavior
    var goTo = this.getAttribute("href"); // store anchor href

    // do something while timeOut ticks ... 

    setTimeout(function(){
         window.location = goTo;
    },1200);
}); 

(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.5";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
   
function carregaFlash(caminho,largura,altura,id){
	document.write('<object style="z-index:-2" id="'+id+'" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="'+largura+'" height="'+altura+'">');
	document.write('<param name="movie" value="'+caminho+'">');
	document.write('<param name="quality" value="best">');
	document.write('<param name="menu" value="false">');
	document.write('<param name="wmode" value="transparent">');
	document.write('<embed style="z-index:-2" id="'+id+'" src="'+caminho+'" quality="best" type="application/x-shockwave-flash" width="'+largura+'" height="'+altura+'" wmode="transparent"></embed>');
	document.write('</object>');
}
